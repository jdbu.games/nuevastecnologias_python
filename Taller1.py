
                                                # TALLER 1


                # PUNTO 1

# Dados los valores ingresados por el usuario
# (base, altura) mostar en pantalla el area de un triangulo

"""base=int(input("ingrese el base del triangulo\n"))
altura=int(input("ingrese la altura del triangulo\n"))
area = base * altura / 2
print("el area es del triangulo es :",area)"""


               # PUNTO 2


# Convertir la cantidad de dolares
# ingresados por el usuario
# a pesos colombianos y mostrar en pantalla el resultado

"""dolares=int(input("ingrese la cantidad de dolares\n"))
valorPesos=int(input("valor en pesos Colombianos\n"))
resultado=dolares*valorPesos
print("el valor es:",resultado)"""


                # PUNTO 3

# convertir los grados centigrados ingresados
# por un usuario a grados fahreinhait y mostrar el resultado
# en pantalla

"""gradosCentigrados=float(input("ingrese los grados centigrados:\n"))
gradosFahrenheit= (gradosCentigrados*(9/5))+32
print(gradosCentigrados ,"Grados centigrados: Los grados centigrados convertidos a grados fahrenheit son  :",gradosFahrenheit , "grados fahrenheit")"""

                # PUNTO 4


# mostrar en pantalla la cantidad de segundos que tiene un lustro

"""años=int(5)
dias=int(365)
horas=int(24)
minutos=int(60)
segundos=int(60)
diaBiciesto =int(86400)
lustro=(segundos*minutos*horas*dias*años) + diaBiciesto
print("la cantidad de segundos que tiene un lustro es:",lustro, " millones de segundos")"""



               # PUNTO 5


#calcular la cantidad de segundos que le toma viajar la luz del sol a marte y mostrarlo en pantalla

"""distanciaSolMarte=227940000 # millones de kilometros
velocidadLuz=300000 # m/s
totalSegundos=int(distanciaSolMarte/velocidadLuz)
minutos = 60
totalMinutos =int(totalSegundos/minutos)
print("la cantidad de segundos que se demora en viajar la luz del sol a marte es de:",totalSegundos, " segundos")
print("la cantidad de minutos  que se demora en viajar la luz del sol a marte es de :",totalMinutos, "minutos")"""


                # PUNTO 6


# calcular el numero de vueltas que da una llanta en 1km, dado que el diametro es de 50cm,
# mostrar el resultado en pantalla

"""centimetros=100000
diametroLlanta=int(50)
totalVueltas=centimetros/diametroLlanta
print("la cantidad de vuelta que dio la llanta es:",totalVueltas, "vueltas")"""



                         # PUNTO 7

# calcular y mostrar en pantalla la longitud de la sombra de un edificio de 20 metros
# de altura cuando el angulo que forman los rayos del sol con el suelo es de 22º

"""import math
altura=20
angulo=float(math.radians(22))
angulo1=math.radians(angulo)
sombra=altura/math.tan(22)
print(sombra)"""

              # PUNTO 8

# mostrar en pantalla True o False si la edad ingresada por dos usuarios es la misma

"""edad1=int(input("ingrese edad uno:\n"))
edad2=int(input("ingrese  edad dos:\n"))
resultado=edad1==edad2
print(resultado)"""


                    # PUNTO/9


# mostrar en pantalla la cantidad de meses trascurridos desde la fecha de nacimiento de un usuario

"""from datetime import date

anio = int(input("Ingrese Año de nacimiento\n"))
mes = int(input("Ingrese mes de nacimiento\n"))
dia = int(input("Ingrese dia de nacimiento\n"))
hoy= date.today()
meses = (hoy.year - anio) *12 + (hoy.month - mes)
print("lacantidad de meses trascurridos desde su fecha de nacimiento son de :", meses)"""



                        # PUNTO 10

# mostrar en pantalla el promedio de un alumno que ha cursado 5 materias
# (español, matematicas, programacion, economia, ingles)

"""español=float(input("nota de Español:\n"))
matematicas=float(input("nota de Matematicas:\n"))
economia=float(input("nota de Economia:\n"))
programacion=float(input("nota de Programacion:\n"))
ingles=float(input("nota de Ingles:\n"))

promedio= (español + matematicas + economia + programacion + ingles)/5
print("tu promedio es",promedio)"""

